(function() {
  'use strict';

  angular
  .module('lrStore',
    [
      'ui.router',
      'ngResource',
      'lrStore.widgets',
      'lrStore.products',
      'lrStore.cart',
      'lrStore.checkout',
      'firebase'
    ])

})();
