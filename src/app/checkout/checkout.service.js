(function() {
  'use strict';

  angular
    .module('lrStore.checkout')
    .factory('Checkout', Checkout);

    Checkout.$inject = ['$window', 'Cart', '$firebaseArray', '$q'];

    function Checkout($window, Cart, $firebaseArray, $q) {
      var ref = firebase.database().ref().child('orders');
      var order = $firebaseArray(ref);

      var service = {};

      service.saveOrder = function(cart, guestUser) {
        vm.order.products = cart;
        vm.order.user = guestUser;

        order.$add(vm.order).then(function(ref) {
          var id = ref.key;
          order.$indexFor(id);
          goToConfirm(vm.order.order);
        });
      }



      return service;
    }
})();
