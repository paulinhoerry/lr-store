(function() {
  'use strict';

  angular
    .module('lrStore.checkout')
    .controller('CheckoutController', CheckoutController)
    .controller('CheckoutConfirmController', CheckoutConfirmController);

    CheckoutController.$inject = ['$state', 'Cart', 'Checkout'];
    CheckoutConfirmController.$inject = ['$stateParams'];

    function CheckoutController($state, $window, Cart, Checkout) {
      var vm = this;


      vm.getItems = Cart.cart;
      vm.saveOrder = saveOrder;
      vm.goToConfirm = goToConfirm;


      function saveOrder(cart, guestUser) {
        Checkout.saveOrder(cart, guestUser);
      }

      function goToConfirm(params) {
        $state.go('checkout-confirm', {order: params});
      }
    }

    function CheckoutConfirmController($stateParams) {
      var vm = this;

      vm.orderNumber = "Ordem enviada com sucesso";
    }
})();
