(function() {
  'use strict';

  angular
    .module('lrStore')
    .config(productsRouteConfig);

    //define routes to app
    var routes = [
      {
        name: 'products',
        config: {
          url: '/products',
          templateUrl: 'src/app/products/products.html',
          controller: 'ProductsController',
          controllerAs: 'vm'
        }
      },
      {
        name: 'details',
        config: {
          url: '/products/:id',
          cache: true,
          templateUrl: 'src/app/products/products-details.html',
          params: {
            name: null,
            id: null
          },
          controller: 'ProductDetailsController',
          controllerAs: 'vm'
        }
      },
      {
        name: 'cart',
        config: {
          url: '/cart',
          cache: true,
          templateUrl: 'src/app/cart/cart.html',
          params: {
            name: null,
            id: null
          },
          controller: 'CartController',
          controllerAs: 'vm'
        }
      },
      {
        name: 'checkout',
        config: {
          url: '/checkout',
          templateUrl: 'src/app/checkout/checkout.html',
          controller: 'CheckoutController',
          controllerAs: 'vm'
        }
      },
      {
        name: 'checkout-confirm',
        config: {
          url: '/checkout/:order',
          templateUrl: 'src/app/checkout/checkout-confirm.html',
          controller: 'CheckoutConfirmController',
          controllerAs: 'vm'
        }
      },
      {
        name: 'men',
        config: {
          url: '/men',
          templateUrl: 'src/app/products/products-men.html',
          controller: 'ProductsController',
          controllerAs: 'products'
        }
      },
      {
        name: 'women',
        config: {
          url: '/women',
          templateUrl: 'src/app/products/products-women.html',
          controller: 'ProductsController',
          controllerAs: 'products'
        }
      },
      {
        name: 'kids',
        config: {
          url: '/kids',
          templateUrl: 'src/app/products/products-kids.html',
          controller: 'ProductsController',
          controllerAs: 'products'
        }
      }
    ];

    /** @ngInject */
    function productsRouteConfig($stateProvider, $urlRouterProvider, $locationProvider) {

      routes.map(function(route, i){
        $stateProvider.state(route.name, route.config);
      })

      $urlRouterProvider.otherwise('/products');
      $locationProvider.html5Mode(true);
    }

})();
