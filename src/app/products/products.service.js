(function() {
  'use strict';

  angular
    .module('lrStore.products')
    .service('Product', Product);

  Product.$inject = ['$http'];

  /** @ngInject */
  function Product($http) {
    var service = {};

    service.getProducts = function() {
      return $http.get('https://liferay-store.firebaseio.com/products.json');
    }

    service.getProductDetails = function(key) {
      return $http.get('https://liferay-store.firebaseio.com/products/' + key + '.json');
    }



    return service;
  }


})();
