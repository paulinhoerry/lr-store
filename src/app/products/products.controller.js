(function() {
  'use strict';

  angular
    .module('lrStore.products')
    .controller('ProductsController', ProductsController)
    .controller('ProductDetailsController', ProductDetailsController);

  ProductsController.$inject = ['$state', 'Product', 'Cart'];
  ProductDetailsController.$inject = ['$stateParams', 'Cart', 'Product'];

  /** @ngInject */
  function ProductsController($state, Product, Cart) {
    var vm = this;

    vm.getProducts = getProducts();
    vm.gotoDetails = gotoDetails;
    vm.addToCart = addToCart;


    function getProducts() {
      Product.getProducts().success(function(data){
        vm.products = data;
      })
    }


    function gotoDetails(params) {
      $state.go('details', params);
    }

    function addToCart(product) {
      Cart.addProd(angular.copy(JSON.stringify(product)));
      Cart.getItems();
    }
  }

  /** @ngInject */
  function ProductDetailsController($stateParams, Cart, $firebaseObject, Product) {
    var vm = this;
    var key = $stateParams.id;

    console.log('key', key);


    function getProductDetails(key) {
      Product.getProductDetails(key).success(function(data) {
          console.log(data);
          vm.product = data;
        })
    }

    vm.product = getProductDetails(key);
    vm.addToCart = addToCart;

    function addToCart() {
      Cart.addProd(angular.toJson(vm.product));
      Cart.getItems();
    }
  }
})();
