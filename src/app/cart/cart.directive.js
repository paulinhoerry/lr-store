(function() {
  'use strict';

  angular
      .module('lrStore.cart')
      .directive('ngCart', ngCart);

  ngCart.$inject = ['Cart'];

  /* @ngInject */
  function ngCart(Cart) {
    var directive;
    directive = {
      templateUrl: 'src/app/cart/cart-cest.html',
      controller: 'CartController',
      link: link
    };

    return directive;

    function link(scope, el, attr) {

      scope.items = Cart.cart;
      scope.cartActive = false;

      el.bind('mouseenter', cartState);

      function cartState() {
        if(!scope.cartActive) {
          scope.cartActive = true;
        } else {
          scope.cartActive = false;
        }
      }

    }
  }
})();
