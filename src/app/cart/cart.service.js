(function() {
  'use strict';

  angular
    .module('lrStore.cart')
    .factory('Cart', Cart);

    Cart.$inject = ['$window'];

    function Cart($window) {
      var service = {};

      service.cart = [];

      service.addProd = function(item){
        $window.localStorage.setItem('item', item);
        var prod = $window.localStorage.getItem('item');

        service.cart.push(JSON.parse(prod));
      }

      service.getItems = function(items) {
        items = service.cart;
        return items;
      }

      service.clear = function() {
        service.cart = [];
        $window.localStorage.clear();
      }

      return service;

    }
})();
