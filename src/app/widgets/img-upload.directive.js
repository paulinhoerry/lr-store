(function() {
  'use strict';

  angular
      .module('lrStore.widgets')
      .directive('imgUpload', imgUpload);

  /* @ngInject */
  function imgUpload() {
    var directive;
    directive = {
      scope: {
        imgUpload: '='
      },
      link: link
    };

    return directive;

    function link(scope, el, attr) {
      el.bind("change", function (changeEvent) {
        var reader = new FileReader();
        reader.onload = function (loadEvent) {
          scope.$apply(function () {
            scope.imgUpload = loadEvent.target.result;
          });
        }
        reader.readAsDataURL(changeEvent.target.files[0]);
      });
    }
  }
})();
