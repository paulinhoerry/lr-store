(function() {
  'use strict';

  angular
    .module('lrStore.checkout')
    .controller('CheckoutController', CheckoutController)
    .controller('CheckoutConfirmController', CheckoutConfirmController);

    CheckoutController.$inject = ['$firebaseArray', '$state', 'Cart', 'Checkout'];
    CheckoutConfirmController.$inject = ['$stateParams'];

    function CheckoutController($firebaseArray, $state, Cart, Checkout) {
      var vm = this;

      var ref = firebase.database().ref().child('orders');
      var order = $firebaseArray(ref);

      vm.getItems = Cart.cart;
      vm.orderNumber = orderNumber;
      vm.saveOrder = saveOrder;
      vm.goToConfirm = goToConfirm;
      vm.order = {
        order: order.length,
        products: [],
        user: {
          name: '',
          address: '',
          state: '',
          pc: null
        }
      }

      orderNumber();

      function orderNumber() {
        order.$loaded().then(function(orders) {
         vm.order.order = orders.length + 1;
       });
      }

      function saveOrder(cart, guestUser) {
        vm.order.products = cart;
        vm.order.user = guestUser;

        order.$add(vm.order).then(function(ref) {
          var id = ref.key;
          order.$indexFor(id);
          goToConfirm(vm.order.order);
        });
      }

      function goToConfirm(params) {
        $state.go('checkout-confirm', {order: params});
      }
    }

    function CheckoutConfirmController($stateParams) {
      var vm = this;

      vm.orderNumber = $stateParams.order;
    }
})();
