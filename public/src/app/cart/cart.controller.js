(function() {
  'use strict';

  angular
    .module('lrStore.cart')
    .controller('CartController', CartController);

    CartController.$inject = ['$window', '$state', 'Cart'];

    function CartController($window, $state, Cart) {
      var vm = this;

      vm.getItems = Cart.cart;
      vm.goToDetails = goToDetails;

      function goToDetails(params) {
        $state.go('details', params);
      }

    }
})();
