(function() {
  'use strict';

  angular
  .module('lrStore',
    [
      'ui.router',
      'lrStore.widgets',
      'lrStore.products',
      'lrStore.cart',
      'lrStore.checkout',
      'firebase'
    ])

})();
