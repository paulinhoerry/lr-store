(function() {
  'use strict';

  angular
    .module('lrStore.products')
    .controller('ProductsController', ProductsController)
    .controller('ProductDetailsController', ProductDetailsController);

  ProductsController.$inject = ['$state', '$firebaseArray', 'Product', 'Cart'];
  ProductDetailsController.$inject = ['$stateParams', 'Cart', '$firebaseObject', 'Product'];

  /** @ngInject */
  function ProductsController($state, $firebaseArray, Product, Cart) {
    var vm = this;
    var productsRef = firebase.database().ref().child('products');
    var productData = $firebaseArray(productsRef);
    vm.products = $firebaseArray(productsRef);

    vm.gotoDetails = gotoDetails;
    vm.addToCart = addToCart;
    vm.productForm = {
      name: '',
      description: '',
      image: '',
      features: []
    };

    function gotoDetails(params) {
      $state.go('details', params);
    }

    function addToCart(product) {
      console.log(product);
      Cart.addProd(angular.copy(JSON.stringify(product)));
      Cart.getItems();
    }
  }

  /** @ngInject */
  function ProductDetailsController($stateParams, Cart, $firebaseObject, Product) {
    var vm = this;
    var key = $stateParams.id;

    vm.product = Product.getProd(key);
    vm.addToCart = addToCart;

    function addToCart() {
      Cart.addProd(angular.toJson(vm.product));
      Cart.getItems();
    }
  }
})();
