(function() {
  'use strict';

  angular
    .module('lrStore.products')
    .factory('Product', Product);

  Product.$inject = ['$firebaseObject'];

  /** @ngInject */
  function Product($firebaseObject) {
    var factory = {};
    var url = "https://liferay-store.firebaseio.com/products"

    factory.getProd = function(productID) {
      var data = firebase.database().ref().child('products').child(productID);
      return $firebaseObject(data);
    }

    return factory;
  }


})();
