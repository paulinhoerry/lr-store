var gulp               = require('gulp');
var concat             = require('gulp-concat');
var uglify             = require('gulp-uglify');
var sourcemaps         = require('gulp-sourcemaps');
var browserSync        = require('browser-sync').create();
var historyApiFallback = require('connect-history-api-fallback');
var sass               = require('gulp-sass');
var postcss            = require('gulp-postcss');
var gmcq               = require('gulp-group-css-media-queries');
var plumber            = require('gulp-plumber');
var sourcemaps         = require('gulp-sourcemaps');
var autoprefixer       = require('autoprefixer');
var rucksack           = require('rucksack-css');
var lost               = require('lost');
var fontMagician       = require('postcss-font-magician');

var nodeModules = 'node_modules/'

var srcApp = {
  js: ['src/app/index.module.js', 'src/app/**/*.module.js', 'src/app/**/*.js'],
  html: 'src/app/index.html',
  css: 'src/style/**/*.scss',
  scss: 'src/style/style.scss',
  vendors: [
    nodeModules + 'angular/angular.min.js',
    nodeModules + 'angular-route/angular-route.min.js',
    nodeModules + 'angular-ui-router/release/angular-ui-router.min.js',
    nodeModules + 'firebase/firebase.js',
    nodeModules + 'angularfire/dist/angularfire.min.js'
  ]
}

var buildApp = {
  build: 'build/**/*',
  js: 'build/js/',
  css: 'build/css/',
  html: 'build/',
};

gulp.task('css', function() {
  return gulp.src(srcApp.scss)
     .pipe(sass({ outputStyle: 'compressed'}))
     .pipe(gmcq())
     .pipe(postcss([rucksack(), lost(), autoprefixer(), fontMagician()]))
     .pipe(plumber())
     .pipe(sourcemaps.init())
     .pipe(concat('style.min.css'))
     .pipe(gulp.dest(buildApp.css));
});

gulp.task('scripts', function() {
  return gulp.src(srcApp.js)
    .pipe(sourcemaps.init())
    .pipe(concat('main.min.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(buildApp.js));
});

gulp.task('vendors', function() {
  return gulp.src(srcApp.vendors)
    .pipe(concat('vendors.min.js'))
    .pipe(gulp.dest(buildApp.js));
});

gulp.task('html', function() {
  return gulp.src(srcApp.html)
    .pipe(gulp.dest(buildApp.html));
});

gulp.task('server', function() {
  var files = [
    buildApp.build
  ];

  browserSync.init(files, {
      server: {
          baseDir: "./",
          middleware: [ historyApiFallback() ]
      }
  });
});

// Rerun the task when a file changes
gulp.task('watch', function() {
  gulp.watch(srcApp.js,   ['scripts']);
  gulp.watch(srcApp.html, ['html']);
  gulp.watch(srcApp.css,  ['css']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch', 'scripts', 'css', 'html', 'vendors', 'server']);
